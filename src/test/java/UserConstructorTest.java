import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class UserConstructorTest {
    String firstname;
    String surname;
    boolean expectedSuccess;

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    public UserConstructorTest(String firstname, String surname, boolean expectedSuccess) {
        this.firstname = firstname;
        this.surname = surname;
        this.expectedSuccess = expectedSuccess;
    }

    @Parameterized.Parameters(name = "{index}: User(\"{0}\", \"{1}\") should be a {2} constructor call")
    public static Object[][] data() {
        return new Object[][]{
                {"first", "second", true},
                {"containing space", "second", true},  // because only first letter of firstname should be used in email
                {"first", "containing space", false},  // because surname should be used in email
                {"", "second", false},
                {"first", "", false},
                {null, "typical", false},
                {"typical", null, false},
        };
    }

    @Test
    public void userConstructorTest() {
        if(!this.expectedSuccess)
            thrown.expect(IllegalArgumentException.class);
        new User(this.firstname, this.surname);
    }
}