import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserFriendshipTest {
    User userUnderTest, anotherUser, thirdUser;

    @Before
    public void setUp() throws Exception {
        this.userUnderTest = new User("first", "ofAll");
        this.anotherUser = new User("second", "user");
        this.thirdUser = new User("third", "user");
    }

    @Test
    public void friendshipOnInitTest() {
        assertFalse(this.userUnderTest.isFriendsWith(this.anotherUser));
    }

    @Test
    public void addFriendTest() {  // test asymmetry
        this.userUnderTest.addFriend(this.anotherUser);
        assertTrue(this.userUnderTest.isFriendsWith(this.anotherUser));
        assertFalse(this.anotherUser.isFriendsWith(this.userUnderTest));
    }

    @Test
    public void addMutualFriendTest() {
        this.userUnderTest.addFriend(this.anotherUser);
        this.anotherUser.addFriend(this.userUnderTest);
        assertTrue(this.userUnderTest.isFriendsWith(this.anotherUser));
        assertTrue(this.anotherUser.isFriendsWith(this.userUnderTest));
    }

    @Test
    public void removeOneSideMutualFriendTest() {  // test asymmetry on remove
        this.userUnderTest.addFriend(this.anotherUser);
        this.anotherUser.addFriend(this.userUnderTest);
        this.userUnderTest.removeFriend(this.anotherUser);
        assertFalse(this.userUnderTest.isFriendsWith(this.anotherUser));
        assertTrue(this.anotherUser.isFriendsWith(this.userUnderTest));
    }

    @Test
    public void addMultipleFriendTest() {  // test transitivity
        this.userUnderTest.addFriend(this.anotherUser);
        this.anotherUser.addFriend(this.thirdUser);
        assertTrue(this.userUnderTest.isFriendsWith(this.anotherUser));
        assertTrue(this.anotherUser.isFriendsWith(this.thirdUser));
        assertFalse(this.userUnderTest.isFriendsWith(this.thirdUser));
    }

    @Test
    public void removeFriendTest() {
        this.userUnderTest.addFriend(this.anotherUser);
        this.userUnderTest.removeFriend(this.anotherUser);
        assertFalse(this.userUnderTest.isFriendsWith(this.anotherUser));
        assertFalse(this.anotherUser.isFriendsWith(this.userUnderTest));
    }

    @Test
    public void removeNonExistingFriendTest() {
        this.userUnderTest.removeFriend(this.anotherUser);
    }
}