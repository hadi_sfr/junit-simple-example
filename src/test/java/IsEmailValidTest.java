import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeNotNull;

@RunWith(Parameterized.class)
public class IsEmailValidTest {
    String email;
    boolean isEmailValid;

    public IsEmailValidTest(String email, boolean isEmailValid) {
        this.email = email;
        this.isEmailValid = isEmailValid;
    }

    @Parameterized.Parameters(name = "{index}: {0} is a {1} email")
    public static Object[][] data() {
        return new Object[][]{
                {"info@example.com", true},
                {"Info@example.com", true},
                {"inf0@example.com", true},
                {"!nfo@example.com", true},
                {"public.affairs@example.com", true},
                {"public-affairs@example.com", true},
                {"public_affairs@example.com", true},
                {"@example.com", false},
                {"user@TL.D", false},
                {"user@domain", false},
                {"user@d", false},
                {"info@", false},
                {"info@subdomain.example.com", true},
                {"info@example-domain.example.com", true},
                {"info@example_domain.example.com", true},
                {"info@exampl3.com", true},
                {"info@examp!e.com", false},
                {"em@!l@example.com", false},
                {"", false},
                {null, false},
        };
    }

    @Test
    public void isEmailValidTest() {
        assertEquals(this.isEmailValid, User.isEmailValid(email));
    }
}