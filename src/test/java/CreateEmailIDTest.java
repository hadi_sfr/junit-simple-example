import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.junit.Assume.*;

@RunWith(Theories.class)
public class CreateEmailIDTest {
    @DataPoints
    public static String[] names() {
        return new String[]{null, "", "j", "jj", "typicalLongString"};
    }

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Theory
    public void createEmailIDTest(String firstPart, String secondPart) {
        System.out.println(String.format("%s %s", firstPart, secondPart));
        if(firstPart == null || secondPart == null ||
                firstPart.length() == 0 || secondPart.length() == 0) {
            thrown.expect(IllegalArgumentException.class);
            System.out.println();
        }
        String actual = User.createEmailID(firstPart, secondPart);
        System.out.println("after createEmailID without failing");
        System.out.println(actual);
        System.out.println();

        String parts[] = actual.split("@");
        assertEquals(2, parts.length);
        assertEquals("test.ut.ac.ir", parts[1]);
        assertEquals(firstPart.charAt(0), parts[0].charAt(0));
        assertEquals('.', parts[0].charAt(1));
        assertEquals(secondPart, parts[0].substring(2));
        assertEquals(String.format("%s.%s@test.ut.ac.ir", firstPart.charAt(0), secondPart), actual);
    }
}